function reset(){
	document.querySelector('#error-message').innerHTML = '';
	document.querySelector('h1').style.color = black;
	document.querySelectorAll('h2, .text-news').forEach(element => element.style.color = black);
}

function highlightSearchFind(elementToSearch, search){
	if(!elementToSearch.innerHTML.toLowerCase().includes(search.toLowerCase())) {
		return 0;
	}

	elementToSearch.style.color = blue;
	return 1;
}

function updateH1(search, isFind){
	let h1 = document.querySelector('h1');
	h1.innerHTML = search;

	if(!isFind)
		h1.style.color = red;
}

function loadNews(array){
	var parentDiv = document.querySelector("#allNews");
	
	array.forEach(function(element){
		var div = document.createElement('div');
		div.className="col-md-4";
		var h2 = document.createElement('h2');
		h2.textContent=element.title;
		var p = document.createElement('p');
		p.className="text-news";
		p.textContent=element.desc;
		var a = document.createElement('a');	
		a.className="btn btn-secondary";
		a.setAttribute("role","button");
		a.textContent="View details";

		div.appendChild(h2);
		div.appendChild(p);
		div.appendChild(a);

		parentDiv.appendChild(div);
	});
}
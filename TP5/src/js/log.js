function logMessage(message) {
	console.log(message);
}

function logHtmlElements(selector){
	let titles = document.querySelectorAll(selector);
	titles.forEach(title => logMessage(title.innerHTML));
}
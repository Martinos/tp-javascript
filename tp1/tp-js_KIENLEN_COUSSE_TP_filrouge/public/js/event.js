var buttons = document.querySelectorAll("a");
buttons.forEach(function(element){
	element.addEventListener('click',function(){
		afficherNews(element);
	});
});

document.querySelector("button").onclick = function(){
	var searchValue = document.querySelector("#search").value;
	var h1 = document.querySelector("h1");
	var span = document.querySelector("#error-message");
	if(searchValue==""){
		errorMessage("Champ vide");
		span.textContent="Champ vide";
	}				
	else{		
		span.textContent="";		
		h1.textContent=searchValue;
		var motNews = document.querySelectorAll(".text-news");	
		motNews.forEach(function(element){
			var codeRetour = element.innerHTML.search(searchValue);
			if(codeRetour != -1) {
				element.setAttribute("style","color:"+bleu);
				h1.setAttribute("style","color:"+noir);
			} else {
				h1.setAttribute("style","color:"+rouge);
				element.setAttribute("style","color:"+noir);
			}
		});
	}			
	return false;
}
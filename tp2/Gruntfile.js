module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    copy: {
	  main: {
	    files: [
	      // includes files within path
	      {expand: false, src: ['node_modules/bootstrap/dist/css/bootstrap.min.css'], dest: 'public/css/bootstrap.min.css', filter: 'isFile'},
	      {expand: false, src: ['node_modules/jquery/dist/jquery.min.js'], dest: 'public/js/jquery.min.js', filter: 'isFile'},
	    ],
	  },
	},

	uglify: {
	    my_target: {
	      files: {
	        'public/js/const.min.js': ['public/js/const.js']
	      }
	    }
  	}

  });

  // Do grunt-related things in here
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['copy:main', 'uglify']);
};

 